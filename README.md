# `Suropati Geo Map` — Geomap for Suropati Web apps

**Before starting Protractor, open a separate terminal window and run:**

```
npm i & npm start 
```

In addition, since Protractor is built upon WebDriver, we need to ensure that it is installed and
up-to-date. The `Suropati Geo Map` project is configured to do this automatically before running the
end-to-end tests, so you don't need to worry about it. If you want to manually update the WebDriver,
you can run: npm install
