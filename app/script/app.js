var app = angular.module("suropati-geo-map", ["ngResource", "lbServices"]);

//Loading Start
app.directive("loading", function () {
  return {
    restrict: "E",
    replace: true,
    template: `
    <div id="preloader">
    <div id="loader"></div>
    </div>
    `,
    link: function (scope, element, attr) {
      scope.$watch("loading", function (val) {
        if (val) $(element).show();
        else $(element).hide();
      });
    },
  };
});
//Loading End

app
  .controller("AkuPunyaControl", function (
    $scope,
    $rootScope,
    $http,
    TKW,
    Week
  ) {
    
    $scope.title_card = "DEVICE SHARE";
    $scope.fields_value = "device_share";
    $scope.type_value = "inc";
    $scope.form_data = { yearweek:''}
    $scope.piechart = [];
    $scope.op = [];
    var yearmax = function yearmax(arr, prop) {
      var max;
      for (var i = 0; i < arr.length; i++) {
        if (!max || parseFloat(arr[i][prop]) > parseFloat(max[prop]))
          max = arr[i];
      }
      return max;
    };
    var q = [];
    Week.find((w) => {
      $scope.form_data = yearmax(w, 'yearweek')
      update(
        $scope.form_data.yearweek,
        $scope.fields_value,
        $scope.type_value,
        $scope.title_card
      );
      w.forEach((v) => {
        q.push(v.yearweek);
      });
      //$scope.form_data = { yearweek: 202040 };
      $scope.weeks = w;
      $scope.prop = {
        type: "select",
        name: $scope.form_data.yearweek,
        value: $scope.form_data.yearweek,
        values: q,
      };
    });
    vm = this;
    $scope.data = [];
    $scope.data29 = [];

    // vm.map = new L.Map("map-canvas", {
    //   center: new L.LatLng(-1.789275, 117.921326),
    //   zoom: 6,
    //   zoomSnap: 1 ,
    //   layers: new L.TileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"),
    //   zoomControl: false,
    // });
    vm.map = new L.Map("map-canvas", {
      center: new L.LatLng(-1.789275, 119.921326),
      zoom: 5.2,
      zoomSnap: 0.1,
      layers: new L.TileLayer(
        "https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoicHJhZHByYXQiLCJhIjoiY2tnMHhwbXZvMDc4eDJ1cXd1ZmFueHk5YiJ9.wfhci5Mpn6cahjx3GnOfYQ"
      ),
      zoomControl: false,
    });
    vm.map.doubleClickZoom.disable(); 
    L.control.zoom({
      position:'bottomright'
    }).addTo(vm.map);
    var stateLayer = new google.maps.Data();
    var desa = [];
    function update(weeks, field_menu, type_set, judul) {
      document.getElementById("kab-detail").style.visibility = "visible";
      $scope.loading = true;

      $scope.t = [];
      $scope.x = [];
      $scope.io = [];
      $scope.s = [];
      $scope.tr = [];
      $scope.t29 = [];
      $scope.x29 = [];
      $scope.io29 = [];
      $scope.s29 = [];
      $scope.tr29 = [];
      $scope.wins = "";
      var data = TKW.find(
        { filter: { where: { yearweek: `${weeks}`, node: "4G" } } },
        (value) => {
          TKW.find(
            { filter: { where: { yearweek: "202029", node: "4G" } } },
            (w29) => {
              AWS(field_menu, w29);
            }
          );
        }
      );
      function AWS(field, value29) {
        function style(feature) {
          var FilDataOperator = data.filter(function (a) {
            if (a.location == feature.properties.KABUPATEN) {
              return a[field];
            }
          });
          $scope.wee = data[0].yearweek.toString().slice(-2);
          maxopp = getMax(FilDataOperator, field);
          try {
            if (maxopp.operator) {
              if (maxopp.operator == "Telkomsel") {
                $scope.t.push(maxopp.operator);
              } else if (maxopp.operator == "XL") {
                $scope.x.push(maxopp.operator);
              } else if (maxopp.operator == "Indosat Ooredoo") {
                $scope.io.push(maxopp.operator);
              } else if (maxopp.operator == "Smartfren") {
                $scope.s.push(maxopp.operator);
              } else if (maxopp.operator == "3") {
                $scope.tr.push(maxopp.operator);
              }
            }
          } catch (error) {
            // return "#DD2616";
          }
          var winer = [
            { op: "Telkomsel", value: $scope.t.length },
            { op: "XL", value: $scope.x.length },
            { op: "Indosat Ooredoo", value: $scope.io.length },
            { op: "Smartfren", value: $scope.s.length },
            { op: "3", value: $scope.tr.length },
          ];
          $scope.wins = getMaxwell(winer, "value");

          var FilDataOperator299 = value29.filter(function (a) {
            if (a.location == feature.properties.KABUPATEN) {
              return a[field];
            }
          });
          maxopp29 = getMax(FilDataOperator299, field);
          try {
            if (maxopp29.operator) {
              if (maxopp29.operator == "Telkomsel") {
                $scope.t29.push(maxopp29.operator);
              } else if (maxopp29.operator == "XL") {
                $scope.x29.push(maxopp29.operator);
              } else if (maxopp29.operator == "Indosat Ooredoo") {
                $scope.io29.push(maxopp29.operator);
              } else if (maxopp29.operator == "Smartfren") {
                $scope.s29.push(maxopp29.operator);
              } else if (maxopp29.operator == "3") {
                $scope.tr29.push(maxopp29.operator);
              }
            }
          } catch (error) {
            // return "#DD2616";
          }
          var winer29 = [
            { op: "Telkomsel", value: $scope.t29.length },
            { op: "XL", value: $scope.x29.length },
            { op: "Indosat Ooredoo", value: $scope.io29.length },
            { op: "Smartfren", value: $scope.s29.length },
            { op: "3", value: $scope.tr29.length },
          ];
          $scope.wins29 = getMaxwell(winer29, "value");
          return {
            fillColor: getColor(feature.properties.KABUPATEN), // call function to get color for state based on KABUPATEN
            weight: 0.5,
            opacity: 1,
            color: "white",
            dashArray: "4",
            fillOpacity: 1,
          };
        }
        // Event Mouse
        $scope.loading = false;
        function mouseout(e) {
          // var layer = e.target;
          
          vm.geojson.resetStyle(e.target);
          e.target.closePopup();
        }

        function mousemove(e) {
          e.target.closePopup();
          var popup = e.target.getPopup();
          popup.setLatLng(e.latlng).openOn(vm.map);
        }

        function mouseover(e) {
          // console.log(e.target)
          var layer = e.target;
          var FilDataOperator = data.filter(function (a) {
            if (a.location == layer.feature.properties.KABUPATEN) {
              return a[field];
            }
          });
          var FilDataOperator29 = value29.filter(function (a) {
            if (a.location == layer.feature.properties.KABUPATEN) {
              return a[field];
            }
          });
          maxop = getMax(FilDataOperator, field);
          maxop29 = getMax(FilDataOperator29, field);
          layer.setStyle({
            weight: 5,
            // color: '#666',
            dashArray: "",
          });
          var datas = data.filter(function (a) {
            if (a.location == layer.feature.properties.KABUPATEN) {
              return a[field];
            }
          });
          var datas29 = value29.filter(function (a) {
            if (a.location == layer.feature.properties.KABUPATEN) {
              return a[field];
            }
          });
          var tsel = data.filter(function (a) {
            if (
              a.location == layer.feature.properties.KABUPATEN &&
              a.operator == "Telkomsel"
            ) {
              return a[field];
            }
          });
          var xl = data.filter(function (a) {
            if (
              a.location == layer.feature.properties.KABUPATEN &&
              a.operator == "XL"
            ) {
              return a[field];
            }
          });
          var sat = data.filter(function (a) {
            if (
              a.location == layer.feature.properties.KABUPATEN &&
              a.operator == "Indosat Ooredoo"
            ) {
              return a[field];
            }
          });
          var sf = data.filter(function (a) {
            if (
              a.location == layer.feature.properties.KABUPATEN &&
              a.operator == "Smartfren"
            ) {
              return a[field];
            }
          });
          var tree = data.filter(function (a) {
            if (
              a.location == layer.feature.properties.KABUPATEN &&
              a.operator == "3"
            ) {
              return a[field];
            }
          });
          var tsel29 = value29.filter(function (a) {
            if (
              a.location == layer.feature.properties.KABUPATEN &&
              a.operator == "Telkomsel"
            ) {
              return a[field];
            }
          });
          var xl29 = value29.filter(function (a) {
            if (
              a.location == layer.feature.properties.KABUPATEN &&
              a.operator == "XL"
            ) {
              return a[field];
            }
          });
          var sat29 = value29.filter(function (a) {
            if (
              a.location == layer.feature.properties.KABUPATEN &&
              a.operator == "Indosat Ooredoo"
            ) {
              return a[field];
            }
          });
          var sf29 = value29.filter(function (a) {
            if (
              a.location == layer.feature.properties.KABUPATEN &&
              a.operator == "Smartfren"
            ) {
              return a[field];
            }
          });
          var tree29 = value29.filter(function (a) {
            if (
              a.location == layer.feature.properties.KABUPATEN &&
              a.operator == "3"
            ) {
              return a[field];
            }
          });

          var popup = L.popup();
          popup.setContent(`
            <div class="card-body">
            <h6 class="title-text">Region</h6>
            <h5 class="detail-text">${layer.feature.properties.REGION}</h5>
            <h6 class="title-text">Kabupaten</h6>
            <h5 class="detail-text">${layer.feature.properties.KABUPATEN}</h5>

            <h6 class="title-text">W29 Winner : </h6>
            <h5 class="detail-text">${maxop29.operator}</h5>
            <h6 class="title-text">W${datas[0].yearweek
              .toString()
              .slice(-2)} Winner :</h6>
            <h5 class="detail-text"> ${maxop.operator}</h5>
           
            <h6 class="title-text">Operator (W29 | W${datas[0].yearweek
              .toString()
              .slice(-2)})</h6>
            <h5 style="margin: 0; font-size: 17px;" class="detail-text"><strong>Telkomsel</strong> : ${
              tsel29[0] ? TypeFields(tsel29[0][field], type_set) : "NA"
            } | ${tsel[0] ? TypeFields(tsel[0][field], type_set) : "NA"}</h5>
            <h5 style="margin: 0; font-size: 17px;" class="detail-text" ><strong>XL</strong> : ${
              xl29[0] ? TypeFields(xl29[0][field], type_set) : "NA"
            } | ${xl[0] ? TypeFields(xl[0][field], type_set) : "NA"}</h5>
            <h5 style="margin: 0; font-size: 17px;" class="detail-text" ><strong>Indosat Ooredoo</strong> : ${
              sat29[0] ? TypeFields(sat29[0][field], type_set) : "NA"
            } | ${sat[0] ? TypeFields(sat[0][field], type_set) : "NA"}</h5>
            <h5 style="margin: 0; font-size: 17px;" class="detail-text" ><strong>Smartfren</strong> : ${
              sf29[0] ? TypeFields(sf29[0][field], type_set) : "NA"
            } | ${sf[0] ? TypeFields(sf[0][field], type_set) : "NA"}</h5>
            <h5 style="margin: 0; font-size: 17px;" class="detail-text" ><strong>3</strong> : ${
              tree29[0] ? TypeFields(tree29[0][field], type_set) : "NA"
            } | ${tree[0] ? TypeFields(tree[0][field], type_set) : "NA"}</h5>
            </div>
          `);
          // <h5 class="detail-text">${TypeFields(datas29[0][field], type_set)} | ${TypeFields(datas[0][field], type_set)}</h5>
          layer.bindPopup(popup);
          popup.setLatLng(e.latlng).openOn(vm.map);
          // this.openPopup();
        }

        function click(e) {
          var layer = e.target;
          var tsel = data.filter(function (a) {
            if (
              a.location == layer.feature.properties.KABUPATEN &&
              a.operator == "Telkomsel"
            ) {
              return a[field];
            }
          });
          var xl = data.filter(function (a) {
            if (
              a.location == layer.feature.properties.KABUPATEN &&
              a.operator == "XL"
            ) {
              return a[field];
            }
          });
          var sat = data.filter(function (a) {
            if (
              a.location == layer.feature.properties.KABUPATEN &&
              a.operator == "Indosat Ooredoo"
            ) {
              return a[field];
            }
          });
          var sf = data.filter(function (a) {
            if (
              a.location == layer.feature.properties.KABUPATEN &&
              a.operator == "Smartfren"
            ) {
              return a[field];
            }
          });
          var tree = data.filter(function (a) {
            if (
              a.location == layer.feature.properties.KABUPATEN &&
              a.operator == "3"
            ) {
              return a[field];
            }
          });
          var tsel29 = value29.filter(function (a) {
            if (
              a.location == layer.feature.properties.KABUPATEN &&
              a.operator == "Telkomsel"
            ) {
              return a[field];
            }
          });
          var xl29 = value29.filter(function (a) {
            if (
              a.location == layer.feature.properties.KABUPATEN &&
              a.operator == "XL"
            ) {
              return a[field];
            }
          });
          var sat29 = value29.filter(function (a) {
            if (
              a.location == layer.feature.properties.KABUPATEN &&
              a.operator == "Indosat Ooredoo"
            ) {
              return a[field];
            }
          });
          var sf29 = value29.filter(function (a) {
            if (
              a.location == layer.feature.properties.KABUPATEN &&
              a.operator == "Smartfren"
            ) {
              return a[field];
            }
          });
          var tree29 = value29.filter(function (a) {
            if (
              a.location == layer.feature.properties.KABUPATEN &&
              a.operator == "3"
            ) {
              return a[field];
            }
          });
          var FilDataOperator = data.filter(function (a) {
            if (a.location == layer.feature.properties.KABUPATEN) {
              return a[field];
            }
          });
          var FilDataOperator29 = value29.filter(function (a) {
            if (a.location == layer.feature.properties.KABUPATEN) {
              return a[field];
            }
          });
          maxop = getMax(FilDataOperator, field);
          maxop29 = getMax(FilDataOperator29, field);
          document.getElementById("detail-kabupaten").innerText =
            layer.feature.properties.KABUPATEN;
          document.getElementById("tsel-value").innerText = tsel[0]
            ? `${tsel[0].operator} : ${numberWithCommas(
                tsel[0].sample
              )} | ${TypeFields(tsel[0][field], type_set)}`
            : "";
          document.getElementById("xl-value").innerText = xl[0]
            ? `${xl[0].operator} : ${numberWithCommas(
                xl[0].sample
              )} | ${TypeFields(xl[0][field], type_set)}`
            : "";
          document.getElementById("sat-value").innerText = sat[0]
            ? `${sat[0].operator} : ${numberWithCommas(
                sat[0].sample
              )} | ${TypeFields(sat[0][field], type_set)}`
            : "";
          document.getElementById("sf-value").innerText = sf[0]
            ? `${sf[0].operator} : ${numberWithCommas(
                sf[0].sample
              )} | ${TypeFields(sf[0][field], type_set)}`
            : "";
          document.getElementById("tree-value").innerText = tree[0]
            ? `${tree[0].operator} : ${numberWithCommas(
                tree[0].sample
              )} | ${TypeFields(tree[0][field], type_set)}`
            : "";
          document.getElementById("tsel-value29").innerText = tsel29[0]
            ? `${tsel29[0].operator} : ${numberWithCommas(
                tsel29[0].sample
              )} | ${TypeFields(tsel29[0][field], type_set)}`
            : "";
          document.getElementById("xl-value29").innerText = xl29[0]
            ? `${xl29[0].operator} : ${numberWithCommas(
                xl29[0].sample
              )} | ${TypeFields(xl29[0][field], type_set)}`
            : "";
          document.getElementById("sat-value29").innerText = sat29[0]
            ? `${sat29[0].operator} : ${numberWithCommas(
                sat29[0].sample
              )} | ${TypeFields(sat29[0][field], type_set)}`
            : "";
          document.getElementById("sf-value29").innerText = sf29[0]
            ? `${sf29[0].operator} : ${numberWithCommas(
                sf29[0].sample
              )} | ${TypeFields(sf29[0][field], type_set)}`
            : "";
          document.getElementById("tree-value29").innerText = tree29[0]
            ? `${tree29[0].operator} : ${numberWithCommas(
                tree29[0].sample
              )} | ${TypeFields(tree29[0][field], type_set)}`
            : "";
          document.getElementById("operator-win").innerText = maxop.operator;
          document.getElementById("operator-win29").innerText =
            maxop29.operator;
          document.getElementById("kab-detail").style.visibility = "visible";
          document.getElementById("detail-region").innerText = tsel[0].region;
          document.getElementById(
            "weekly-top"
          ).innerText = data[0].yearweek.toString().slice(-2);
          document.getElementById(
            "weekly-bot"
          ).innerText = data[0].yearweek.toString().slice(-2);
        }

        function onEachFeature(feature, layer) {
          layer.on({
            mouseover: mouseover,
            mouseout: mouseout,
            mousemove: mousemove,
            click: click,
          });
        }
        vm.geojson = L.geoJson(kabupaten_geo, {
          style: style,
          onEachFeature: onEachFeature,
        }).addTo(vm.map);

        

        function getColor(idkab) {
          var FilDataOperator = data.filter(function (a) {
            if (a.location == idkab && a.node == "4G") {
              return a[field];
            }
          });
          maxop = getMax(FilDataOperator, field);
          try {
            if (maxop.operator) {
              if (maxop.operator == "Telkomsel") {
                return "#DD2616";
              } else if (maxop.operator == "XL") {
                return "#2F6BB4";
              } else if (maxop.operator == "Indosat Ooredoo") {
                return "#FFD50F";
              } else if (maxop.operator == "Smartfren") {
                return "#A944B5";
              } else if (maxop.operator == "3") {
                return "#383838";
              }
            }
          } catch (error) {
            return "transparent";
          }
        }
        $scope.loading = false;
      }
    }
    $scope.up = function (a) {
      $scope.form_data.yearweek = a;
      update(
        $scope.form_data.yearweek,
        $scope.fields_value,
        $scope.type_value,
        $scope.title_card
      );
    };
    

    function reload_addlist(op) {
      $scope.op = op;
    }
    var numberWithCommas = function numberWithCommas(x) {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };
    var getMax = function getMax(arr, prop) {
      if ($scope.type_value == "ms") {
        var max;
        for (var i = 0; i < arr.length; i++) {
          if (!max || parseFloat(arr[i][prop]) < parseFloat(max[prop]))
            max = arr[i];
        }
        return max;
      } else if ($scope.type_value == "inc") {
        var max;
        for (var i = 0; i < arr.length; i++) {
          //console.log(`${(parseFloat(arr[i][prop]).toFixed(2))*100} > ${(parseFloat(max[prop]).toFixed(2))*100}`)
          if (
            !max ||
            parseFloat(arr[i][prop]).toFixed(2) * 100000 >
              parseFloat(max[prop]).toFixed(2) * 100000
          )
            max = arr[i];
        }
        return max;
      } else {
        var max;
        for (var i = 0; i < arr.length; i++) {
          if (!max || parseFloat(arr[i][prop]) > parseFloat(max[prop]))
            max = arr[i];
        }
        return max;
      }
    };
    var getMaxwell = function getMaxwell(arr, prop) {
      var max;
      for (var i = 0; i < arr.length; i++) {
        if (!max || parseFloat(arr[i][prop]) > parseFloat(max[prop]))
          max = arr[i];
      }
      return max;
    };
    var TypeFields = function TypeFields(v, t) {
      t = $scope.type_value;
      if (v) {
        if (t == "inc") {
          return (v * 100).toFixed(2) + "%";
        } else if (t == "ms") {
          return (v * 1).toFixed(2) + "ms";
        } else if (t == "km") {
          return `${(v * 1).toFixed(2)}km2`;
        } else if (t == "number") {
          return (v * 1).toFixed(2) + "%";
        } else if (t == "kbps") {
          return `${(v * 1).toFixed(2)}Kbps`;
        }
      }
    };
    var sudahlah = function sudahlah(input) {
      input = input || "";
      return input.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      });
    };
    function resetMenu() {
      $(".btn.btn-light.btn-lg.btn-menu.menu-active").each(function () {
        $(this).removeClass("menu-active");
        // console.log($(this).html())
      });
    }

    function activateMenu(type) {
      $("button[ng-click*='" + type + "']").each(function () {
        $(this).addClass("menu-active");
        console.log($(this).html());
      });
    }
    $scope.DS = function (x, y, z) {
      resetMenu();
      activateMenu("device_share");
      $scope.type_value = z;
      $scope.fields_value = y;
      $scope.title_card = x;
      update(
        $scope.form_data.yearweek,
        $scope.fields_value,
        $scope.type_value,
        $scope.title_card
      );
    };
    $scope.GC = function (x, y, z) {
      resetMenu();
      activateMenu("grid_signal_good");
      $scope.type_value = z;
      $scope.fields_value = y;
      $scope.title_card = x;
      update(
        $scope.form_data.yearweek,
        $scope.fields_value,
        $scope.type_value,
        $scope.title_card
      );
    };
    $scope.GQ = function (x, y, z) {
      resetMenu();
      activateMenu("good_quality");
      $scope.type_value = z;
      $scope.fields_value = y;
      $scope.title_card = x;
      update(
        $scope.form_data.yearweek,
        $scope.fields_value,
        $scope.type_value,
        $scope.title_card
      );
    };
    $scope.EQ = function (x, y, z) {
      resetMenu();
      activateMenu("excellent_quality");
      $scope.type_value = z;
      $scope.fields_value = y;
      $scope.title_card = x;
      update(
        $scope.form_data.yearweek,
        $scope.fields_value,
        $scope.type_value,
        $scope.title_card
      );
    };
    $scope.GP = function (x, y, z) {
      resetMenu();
      activateMenu("game_parameter");
      $scope.type_value = z;
      $scope.fields_value = y;
      $scope.title_card = x;
      update(
        $scope.form_data.yearweek,
        $scope.fields_value,
        $scope.type_value,
        $scope.title_card
      );
    };
    $scope.AUT = function (x, y, z) {
      resetMenu();
      activateMenu("avg_upload_throughput");
      $scope.type_value = z;
      $scope.fields_value = y;
      $scope.title_card = x;
      update(
        $scope.form_data.yearweek,
        $scope.fields_value,
        $scope.type_value,
        $scope.title_card
      );
    };
    $scope.AL = function (x, y, z) {
      resetMenu();
      activateMenu("avg_latency");
      $scope.type_value = z;
      $scope.fields_value = y;
      $scope.title_card = x;
      update(
        $scope.form_data.yearweek,
        $scope.fields_value,
        $scope.type_value,
        $scope.title_card
      );
    };
    $scope.AJ = function (x, y, z) {
      resetMenu();
      activateMenu("AJ");
      $scope.type_value = z;
      $scope.fields_value = y;
      $scope.title_card = x;
      update(
        $scope.form_data.yearweek,
        $scope.fields_value,
        $scope.type_value,
        $scope.title_card
      );
    };
    $scope.APL = function (x, y, z) {
      resetMenu();
      activateMenu("APL");
      $scope.type_value = z;
      $scope.fields_value = y;
      $scope.title_card = x;
      update(
        $scope.form_data.yearweek,
        $scope.fields_value,
        $scope.type_value,
        $scope.title_card
      );
    };
    $scope.ES = function (x, y, z) {
      resetMenu();
      activateMenu("enodeb_share");
      $scope.type_value = z;
      $scope.fields_value = y;
      $scope.title_card = x;
      update(
        $scope.form_data.yearweek,
        $scope.fields_value,
        $scope.type_value,
        $scope.title_card
      );
    };
    $scope.CH = function (x, y, z) {
      resetMenu();
      activateMenu("coverage_km2");
      $scope.type_value = z;
      $scope.fields_value = y;
      $scope.title_card = x;
      update(
        $scope.form_data.yearweek,
        $scope.fields_value,
        $scope.type_value,
        $scope.title_card
      );
    };
    $scope.HRQ = function (x, y, z) {
      resetMenu();
      activateMenu("hd_quality");
      $scope.type_value = z;
      $scope.fields_value = y;
      $scope.title_card = x;
      update(
        $scope.form_data.yearweek,
        $scope.fields_value,
        $scope.type_value,
        $scope.title_card
      );
    };
    $scope.VS = function (x, y, z) {
      resetMenu();
      activateMenu("video_score");
      $scope.type_value = z;
      $scope.fields_value = y;
      $scope.title_card = x;
      update(
        $scope.form_data.yearweek,
        $scope.fields_value,
        $scope.type_value,
        $scope.title_card
      );
    };
    $scope.ADT = function (x, y, z) {
      resetMenu();
      activateMenu("avg_download_throughput");
      $scope.type_value = z;
      $scope.fields_value = y;
      $scope.title_card = x;
      update(
        $scope.form_data.yearweek,
        $scope.fields_value,
        $scope.type_value,
        $scope.title_card
      );
    };
  })
  .filter("setdah", function () {
    return function (input) {
      input = input || "";
      return input.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      });
    };
  })
  .filter("setisdua", function () {
    return function (xx) {
      input = xx === 0 ? 0 : xx;
      return input.toFixed(2);
    };
  });

  

  // var obj = $.getJSON("app\shape\kabupaten.json");
  var data_kabupaten = kabupaten_geo['features']
  
  function zoom_to_kabupaten(index) {
    var data = data_kabupaten[index]['properties']
    document.getElementById("search-kabupaten").value = data["KABUPATEN"]

    console.log(data['LONGITUDE']);
    vm.map.setView([data['LATITUDE'], data['LONGITUDE']], 8);
    console.log()
    vm.geojson.getLayers().forEach(element => {
      if (data["KABUPATEN"]==element["feature"]["properties"]["KABUPATEN"]) {
        element.setStyle({
          weight: 5,
          // color: '#666',
          dashArray: "",
        });
        console.log(element["feature"]["properties"]["KABUPATEN"]);
      }
    });

  }


  function autocomplete(inp, arr) {
    var currentFocus;
    inp.addEventListener("input", function(e) {
        var a, b, i, val = this.value;
        closeAllLists();
        if (!val) { return false;}
        currentFocus = -1;
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        this.parentNode.appendChild(a);
        n=0
        for (i = 0; i < arr.length; i++) {
          nama_kabupaten = arr[i]["properties"]["KABUPATEN"]
          // if (nama_kabupaten.substr(0, val.length).toUpperCase() == val.toUpperCase() && n<6) {
          if (nama_kabupaten.toUpperCase().includes(val.toUpperCase())  && n<6) {
            b = document.createElement("div");
            b.setAttribute('onclick', 'zoom_to_kabupaten('+i+')');
            // b.innerHTML = "<strong>" + nama_kabupaten.substr(0, val.length) + "</strong>";
            b.innerHTML += nama_kabupaten;
            b.innerHTML += "<input type='hidden' value='" + nama_kabupaten + "'>";
            a.appendChild(b);
            n=n+1
          }
        }
    });
    inp.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
          currentFocus++;
          addActive(x);
        } else if (e.keyCode == 38) { //up
          currentFocus--;
          addActive(x);
        } else if (e.keyCode == 13) {
          e.preventDefault();
          if (currentFocus > -1) {
            if (x) x[currentFocus].click();
          }
        }
    });
    function addActive(x) {
      if (!x) return false;
      removeActive(x);
      if (currentFocus >= x.length) currentFocus = 0;
      if (currentFocus < 0) currentFocus = (x.length - 1);
      x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
      for (var i = 0; i < x.length; i++) {
        x[i].classList.remove("autocomplete-active");
      }
    }
    function closeAllLists(elmnt) {
      var x = document.getElementsByClassName("autocomplete-items");
      for (var i = 0; i < x.length; i++) {
        if (elmnt != x[i] && elmnt != inp) {
          x[i].parentNode.removeChild(x[i]);
        }
      }
    }
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
  }